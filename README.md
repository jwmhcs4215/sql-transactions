# SQL Transactions

This example shows how to read a collection of data from a csv file into a Spark DataFrame.

Various examples of querying the imported data via DataFrame api as well as SQL queries as shown.

Finally, saving the results of a query to a JSON file on disk is also demonstrated.