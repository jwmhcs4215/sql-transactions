// the following 2 lines are not required if ran in spark-shell (spark session and spark context are already create)
//import org.apache.spark.sql.SparkSession
//val spark = SparkSession.builder.appName("SQL Transaction").getOrCreate()

// load a CSV file into a DataFrame
// this works with ; as separator, with the source file in same dir
val transactDF = spark.read.format("csv").
  option("sep", ";").
  option("inferSchema", "true").
  option("header", "true").
  load("transactionLog.csv")

// we can then directly perform data-processing via DataFrame api
// e.g. Find the unique names of all customers
println("\nUnique names of all customers:")
transactDF.select("name").distinct.show() // outputs to spark-shell

// we can also assign the resultant DataFrame as a new value
// e.g. Find the number of transactions from each country
val transactNumPerCountry = transactDF.groupBy("country").count()

// the value can then be shown via the shell or saved to a file
println("\nNumber of transactions per country:")
transactNumPerCountry.show()
 
// we can also use SQL queries
// first, we create a SQL temporary view from the DataFrame, from which we can perform SQL queries
transactDF.createOrReplaceTempView("transactions")

// e.g. Find the names of customers from Singapore with their respective total spendings converted to USD, sorted based on spendings
val sqlResults = spark.sql("SELECT name, SUM(amount) * 1.31 as total_amount_in_USD FROM transactions WHERE country = 'Singapore' GROUP BY name ORDER BY total_amount_in_USD DESC")

// we can also show a specific number of rows, e.g. show the top 3 spenders from the SQL query
println("\nTop 3 spenders from Singapore:")
sqlResults.show(3)

// here, we save the SQL query result DataFrame into .json format on disk
// this saves the file into a folder named "transactionResults-json", and rerunning this code overwrites any previous results
sqlResults.
  coalesce(1).
  write.format("json").
  mode("overwrite").
  save("transactionResults-json")